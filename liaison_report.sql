-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u8
-- http://www.phpmyadmin.net

-- --------------------------------------------------------

--
-- Table structure for table `liaison_report`
--

CREATE TABLE IF NOT EXISTS `liaison_report` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `receipt_no` int(11) NOT NULL,
  `customer_firstname` varchar(50) NOT NULL,
  `customer_lastname` varchar(50) NOT NULL,
  `slab` int(11) NOT NULL,
  `frame` int(11) NOT NULL,
  `roof` int(11) NOT NULL,
  `rough_in` text NOT NULL,
  `completion_date` text NOT NULL,
  `out_date` text NOT NULL,
  `created_date` text NOT NULL,
  UNIQUE KEY `report_id` (`report_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
