<?php
class Jaui_LiaisonReport_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
            $this->loadLayout();
            $this->renderLayout();
    }

	 public function savepostAction()
    {         

        if ( $this->getRequest()->getPost() ) {

        	
            $postData = $this->getRequest()->getPost();
			$updatemodel = Mage::getModel('liaisonreport/update')->load($postData['ornumber']);
                
			$receiptno = $updatemodel->getReceiptNo();

			if($receiptno == $postData['ornumber']):
				
	            $updatemodel
	                        ->setCustomerFirstname($postData['firstname'])
	                        ->setCustomerLastname($postData['lastname'])
	                        ->setSlab($postData['sel_slab'])
	                        ->setFrame($postData['sel_frame'])
	                        ->setRoof($postData['sel_roof'])  
	                        ->setRoughIn($postData['roughin'])  
	                        ->setCompletionDate($postData['completiondate'])
	                        ->setOutDate($postData['outdate'])                                                                                                                    
	                        ->setCreatedDate('--')
	                        ->save();

				//echo "Data updated successfully.";
                $this->_redirect("liaisonreport/view/index");
			
			else:

	            $insertModel = Mage::getModel('liaisonreport/report');          
	            $insertModel
	                        ->setReceiptNo($postData['ornumber'])
	                        ->setCustomerFirstname($postData['firstname'])
	                        ->setCustomerLastname($postData['lastname'])
	                        ->setSlab($postData['sel_slab'])
	                        ->setFrame($postData['sel_frame'])
	                        ->setRoof($postData['sel_roof'])  
	                        ->setRoughIn($postData['roughin'])  
	                        ->setCompletionDate($postData['completiondate'])
	                        ->setOutDate($postData['outdate'])                                                                                                                    
	                        ->setCreatedDate('--')
	                        ->save();

	            
	            //echo "Data added successfully."; 
                $this->_redirect("liaisonreport/view/index");
			endif;

        }
        
    }
}